# https://oil-dashboards.herokuapp.com/

import pandas as pd
import requests

# 1. waluty bieżące

response = requests.get("http://api.nbp.pl/api/exchangerates/tables/C?format=json").json()
rates = response[0]['rates']
waluty = pd.json_normalize(rates)

dolar_mid = round((waluty['bid'][0] + waluty['ask'][0]) / 2, 4)
dolar_bid = round(waluty['bid'][0], 2)
dolar_ask = round(waluty['ask'][0], 2)
euro_mid = round((waluty['bid'][3] + waluty['ask'][3]) / 2, 4)
gbp_mid = round((waluty['bid'][6] + waluty['ask'][6]) / 2, 4)
aud_mid = round((waluty['bid'][1] + waluty['ask'][1]) / 2, 4)

# zmienne: dolar_mid, dolar_bid, dolar_ask, euro_mid, gbp_mid, aud_mid


# 2. nbp waluty archiwalne

# https://www.nbp.pl/home.aspx?f=/kursy/arch_a.html
# tu mam zagwozdkę, bo wczytuje ręcznie z csv a pownno być aktualne z sieci, rok może ulec zmianie,
# za chwilę będą potrzebne dane za 2023


# df2022 = pd.read_csv("data/publ_sredni_m_2022.csv", sep = ';',  encoding = 'ISO-8859-1', decimal=',')
# df2022 = df2022.drop(columns=['kod', 'liczba jednostek'])
# df2022.columns = ['index', '01.2022', '02.2022', '03.2022', '04.2022', '05.2022', '06.2022', '07.2022',
#                   '08.2022', '09.2022', '10.2022', '11.2022', '12.2022']
# df2022 = df2022.set_index('index')
# df2022.swapaxes(axis1="index", axis2="columns")
# df2022 = df2022.T.reset_index()
# df2022.rename(columns = {'index':'date', 'dolar amerykañski':'usd'}, inplace = True)
# currency = df2022[[ 'date', 'euro', "usd"]]
# currency['date'] = pd.to_datetime(currency['date'], format= '%m.%Y')
# currency = currency.melt(id_vars=['date'], var_name='currency', value_name='price')

currency = pd.read_csv("static/data/currency.csv")
currency = currency.query('date > "2022" ').dropna()

dolar = currency['usd'].tolist()
euro = currency['euro'].tolist()
aud = currency['dolar australijski'].tolist()
gbp = currency['funt szterling'].tolist()

dolar.append(dolar_mid)
euro.append(euro_mid)
aud.append(aud_mid)
gbp.append(gbp_mid)
dolar_start = dolar[0]
dolar_change = round(dolar_mid - dolar_start, 4)

# zmienne: dolar, euro, aud, gbp, dolar_start, dolar_change



# 3. paliwa unia wykres

df1 = pd.read_html('https://www.reflex.com.pl/ceny-detaliczne-europa')
df = df1[0]
df.columns = 'province', '95', 'on', 'lpg', '951', 'on', 'lpg'
df.province = ['Austria', 'Belgium', 'Bulgaria', 'Croatia', 'Cyprus', 'Czech Republic', 'Denmark', 'Estonia', 'Finland', 'France', 'Greece',
'Spain', 'Netherlands', 'Ireland', 'Lithuania', 'Luxembourg', 'Malta', 'Germany', 'Poland', 'Portugal', 'Romania', 'Sweden',
'Slovakia', 'Slovenia', 'Hungary', 'Italy', 'Latvia']

df6 = pd.read_csv('coutries.csv')

_data = df.merge(df6, on='province', how='outer')
_data.fillna(0, inplace=True)

# zmienne: _data


# 3. paliwa hurt

df2 = pd.read_html('https://www.reflex.com.pl/hurtowe-ceny-paliw-w-pkn-orlen-i-grupie-lotos', thousands=None,
                   decimal=',')

# tabela ceny
lotos = df2[0]
lotos.columns = 'paliwo', 'price', 'change'
lotos['litr'] = lotos.iloc[:, 1] / 1000
hurt95 = lotos.litr[0]
hurt98 = lotos.litr[1]
hurton = lotos.litr[2]

# tabela podatki

hurt = df2[2]
hurt.drop(index=hurt.index[0], axis=0, inplace=True)
hurt.columns = ['Paliwo', 'poj', 'Akcyza', 'Opłata paliwowa', 'Opłata zapasowa', 'Opłata emisyjna']
hurt = hurt.replace('-', 0)
hurt = hurt.drop(hurt.columns[1], axis=1)
hurt_melted = hurt.melt(id_vars=['Paliwo'], var_name='Elements', value_name='Value')
hurt_melted.Value = pd.to_numeric(hurt_melted.Value)
hurt_melted.Value = round(hurt_melted.Value / 1000, 2)
h95 = hurt_melted.query('Paliwo == "Benzyna bezołowiowa"')

# lpg hurt
df11 = pd.read_html('https://www.orlenpaliwa.com.pl/PL/notowania/Strony/Autogazhurt.aspx', thousands=None,
                    decimal=',', skiprows=2)
lpg_hurt = df11[0]
lpg_hurt.columns = 'province', 'lpg', 'change'
lpg_hurt = lpg_hurt.lpg.mean()


# zmienne: h95, lpg_hurt, hurt95, hurt98, hurton



# 3. paliwa detal wykres

# wykres ceny
df3 = pd.read_html('https://www.reflex.com.pl/ceny-detaliczne-polska')
polska = df3[0]
polska.columns = 'province', '95', '98', 'ON', 'lpg'
polska.province = polska.province.str.replace(" ", "")
polska = polska.iloc[:-1, :]


# wykres podatki
n = df3[2]
n = n.assign(Type=['95', 'ON'])
n = n.drop(n.columns[0], axis=1)
n_melted = n.melt(id_vars=['Type'], var_name='Elements', value_name='Value')
n95 = n_melted.query('Type == "95"')

# paliwa detal ceny

df = pd.read_html('https://www.autocentrum.pl/paliwa/ceny-paliw/', thousands=None, decimal=',')

autocentrum = df[0]

pb95 = autocentrum["95"][16]
pb98 = autocentrum["98"][16]
on = autocentrum["ON"][16]
lpg = autocentrum["LPG"][16]

# zmienne: polska, n95. pb95, pb98, on, lpg


