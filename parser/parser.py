import math
import nltk
import sys
import os

FILE_MATCHES = 1
SENTENCE_MATCHES = 1


def main():
    # Check command-line arguments
    if len(sys.argv) != 2:
        sys.exit("Usage: python parser.py corpus")

    # Calculate IDF values across files
    files = load_files(sys.argv[1])
    file_words = {
        filename: tokenize(files[filename])
        for filename in files
    }
    file_idfs = compute_idfs(file_words)

    # Prompt user for query
    query = set(tokenize(input("Query: ")))

    # Determine top file matches according to TF-IDF
    filenames = top_files(query, file_words, file_idfs, n=FILE_MATCHES)

    # Extract sentences from top files
    sentences = dict()
    for filename in filenames:
        for passage in files[filename].split("\n"):
            for sentence in nltk.sent_tokenize(passage):
                tokens = tokenize(sentence)
                if tokens:
                    sentences[sentence] = tokens

    # Compute IDF values across sentences
    idfs = compute_idfs(sentences)

    # Determine top sentence matches
    matches = top_sentences(query, sentences, idfs, n=SENTENCE_MATCHES)
    for match in matches:
        print(match)


def load_files(directory):
    """
    Given a directory name, return a dictionary mapping the filename of each
    `.txt` file inside that directory to the file's contents as a string.
    """
    map_dict = {}
    for file_name in os.listdir(directory):
        if file_name.endswith('.txt'):
            with open(os.path.join(directory, file_name), 'r', encoding='utf8') as file:
                map_dict[file_name[:-4]] = file.read()
    return map_dict


def tokenize(document):
    """
    Given a document (represented as a string), return a list of all the
    words in that document, in order.

    Process document by coverting all words to lowercase, and removing any
    punctuation or English stopwords.
    """
    words = nltk.word_tokenize(document.lower())
    stopwords = nltk.corpus.stopwords.words("english")
    return [word for word in words if word.isalpha() and word not in stopwords]


def compute_idfs(documents):
    """
    Given a dictionary of `documents` that maps names of documents to a list
    of words, return a dictionary that maps words to their IDF values.
    Any word that appears in at least one of the documents should be in the
    resulting dictionary.
    """
    word_count = {}
    doc_dict = {}
    for words in documents.values():
        for word in set(words):
            word_count[word] = word_count.get(word, 0) + 1
    for word, count in word_count.items():
        doc_dict[word] = math.log(len(documents) / count) + 1
    return doc_dict


def top_files(query, files, idfs, n):
    """
    Given a `query` (a set of words), `files` (a dictionary mapping names of
    files to a list of their words), and `idfs` (a dictionary mapping words
    to their IDF values), return a list of the filenames of the `n` top
    files that match the query, ranked according to tf-idf.
    """
    scores = {}
    for file, words in files.items():
        score = 0
        for word in query:
            freq = words.count(word) / len(words)
            idf = idfs.get(word, 1)
            score += freq * idf
        scores[file] = score
    return sorted(scores, key=scores.get, reverse=True)[:n]


# start query
def top_sentences(query, sentences, idfs, n):
    """
    Given a `query` (a set of words), `sentences` (a dictionary mapping
    sentences to a list of their words), and `idfs` (a dictionary mapping words
    to their IDF values), return a list of the `n` top sentences that match
    the query, ranked according to idf. If there are ties, preference should
    be given to sentences that have a higher query term density.
    """
    # init result
    result = {}

    # For each sentence
    for sentence in sentences:
        result[sentence] = {}
        result[sentence]['idf'] = 0
        result[sentence]['wc'] = 0
        # And each word within
        for word in query:
            # If word is in sentence, add to wordcount and idf
            if word in sentences[sentence]:
                result[sentence]['idf'] += idfs[word]
                result[sentence]['wc'] += 1
        # Add normed term density
        result[sentence]['dens'] = float(result[sentence]['wc'] / len(sentences[sentence]))

    # Return a sorted list of n top results found by slicing
    return sorted(result.keys(),
                  key=lambda s: (result[s]['idf'], result[s]['dens']),
                  reverse=True)[:n]


if __name__ == "__main__":
    main()
